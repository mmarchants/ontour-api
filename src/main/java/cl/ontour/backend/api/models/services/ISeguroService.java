package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Seguro;
import java.util.List;

public interface ISeguroService {
    
    public List<Seguro> listar();
    public Seguro consultar(Long id);    
    public String registrar(Seguro seguro);    
    public String modificar(Long id, Seguro seguro);    
    public String eliminar(Long id);
    public Seguro consultarCodigo(String codigo);
    
}
