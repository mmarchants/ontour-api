package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.Usuario;
import cl.ontour.backend.api.models.services.IUsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    
    @Autowired
    private IUsuarioService service;
    
    @GetMapping
    public List<Usuario> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Usuario consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/user/{correo}")
    public Usuario obtenerPorCorreo(@PathVariable(value = "correo") String correo) {
        return service.obtenerPorCorreo(correo);
    }
    
    @PostMapping
    public String registrar(@RequestBody Usuario usuario) {
        return service.registrar(usuario);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody Usuario usuario) {
        return service.modificar(id, usuario);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
