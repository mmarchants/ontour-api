package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Pais;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IPaisRepository extends CrudRepository<Pais, Long>{
    
    @Query("SELECT p FROM Pais p WHERE UPPER(p.codigo) = :codigo")
    public Pais findByCodigo(@Param("codigo") String codigo);
}