package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.TipoSeguro;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ITipoSeguroRepository extends CrudRepository<TipoSeguro, Long>{
    
    @Query("SELECT t FROM TipoSeguro t WHERE UPPER(t.codigo) = :codigo")
    public TipoSeguro findByCodigo(@Param("codigo") String codigo);  
    
}
