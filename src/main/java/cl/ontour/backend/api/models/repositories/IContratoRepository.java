package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Colegio;
import cl.ontour.backend.api.models.entities.Contrato;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IContratoRepository extends CrudRepository<Contrato, Long> {
    @Query("SELECT c FROM Contrato c WHERE UPPER(c.codigo) = :codigo")
    public Contrato findByCodigo(@Param("codigo") String codigo);  
}
