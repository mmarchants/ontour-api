package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.TipoContrato;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ITipoContratoRepository extends CrudRepository<TipoContrato, Long> {
    @Query("SELECT t FROM TipoContrato t WHERE UPPER(t.codigo) = :codigo")
    public TipoContrato findByCodigo(@Param("codigo") String codigo);  
}
