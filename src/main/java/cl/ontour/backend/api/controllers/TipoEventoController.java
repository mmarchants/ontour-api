package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.TipoEvento;
import cl.ontour.backend.api.models.services.ITipoEventoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/tipo_evento")
public class TipoEventoController {
    
    @Autowired
    private ITipoEventoService service;
    
    @GetMapping
    public List<TipoEvento> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public TipoEvento consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/codigo/{codigo}")
    public TipoEvento consultarCodigo(@PathVariable(value = "codigo") String codigo) {
        return service.consultarCodigo(codigo);
    }
    
    @PostMapping
    public String registrar(@RequestBody TipoEvento tipo_evento) {
        return service.registrar(tipo_evento);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody TipoEvento tipo_evento) {
        return service.modificar(id, tipo_evento);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
