package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IUsuarioRepository extends CrudRepository<Usuario, Long> {
    
    @Query("SELECT u FROM Usuario u WHERE u.correo_electronico = :correo")
    public Usuario findByCorreoElectronico(@Param("correo") String correo_electronico);
    
}
