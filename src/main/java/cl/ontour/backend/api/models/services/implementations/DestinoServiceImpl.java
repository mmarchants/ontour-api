package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Destino;
import cl.ontour.backend.api.models.repositories.IDestinoRepository;
import cl.ontour.backend.api.models.services.IDestinoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DestinoServiceImpl implements IDestinoService {
    
    @Autowired
    private IDestinoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Destino> listar() {
        return (List<Destino>)repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Destino consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Destino consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Destino destino) {
        repository.save(destino);
        if (repository.findById(destino.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Destino destino) {
        Destino nuevo_destino = consultar(id);
        nuevo_destino.setCodigo(destino.getCodigo());
        nuevo_destino.setNombre(destino.getNombre());
        nuevo_destino.setDescripcion(destino.getDescripcion());
        nuevo_destino.setEnlace_imagen(destino.getEnlace_imagen());
        nuevo_destino.setPais(destino.getPais());
        nuevo_destino.setEstado(destino.getEstado());
        return registrar(nuevo_destino);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Destino destino = consultar(id);
        destino.setEstado(false);
        return registrar(destino);
    }
}
