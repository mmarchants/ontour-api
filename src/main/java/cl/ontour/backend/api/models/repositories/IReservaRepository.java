package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Reserva;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IReservaRepository extends CrudRepository<Reserva, Long> {
    @Query("SELECT r FROM Reserva r WHERE UPPER(r.codigo) = :codigo")
    public Reserva findByCodigo(@Param("codigo") String codigo);  
}
