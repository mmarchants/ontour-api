package cl.ontour.backend.api.models.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Reserva implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_reserva")
    private Long id;
    private String codigo;
    @Lob
    private String detalle;
    private int valor;
    private int monto_aval;
    private int monto_acumulado;
    @Temporal(TemporalType.DATE)
    private Date fecha_viaje;
    @Temporal(TemporalType.DATE)
    private Date fecha_limite;
    private byte participantes;
    @ManyToOne
    @JoinColumn(name = "id_contrato", referencedColumnName = "id_contrato")
    private Contrato contrato;
    @ManyToOne
    @JoinColumn(name = "id_ejecutivo", referencedColumnName = "id_empleado")
    private Empleado ejecutivo;
    @ManyToOne
    @JoinColumn(name = "id_curso", referencedColumnName = "id_curso")
    private Curso curso;
    @ManyToOne
    @JoinColumn(name = "id_programa", referencedColumnName = "id_programa")
    private Programa programa;
    private boolean estado;
    @ManyToMany
    @JoinTable(name = "seguro_reserva", 
            joinColumns = { @JoinColumn(name = "id_reserva") },
            inverseJoinColumns = { @JoinColumn(name = "id_seguro") })
    private List<Seguro> seguros;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getMonto_aval() {
        return monto_aval;
    }

    public void setMonto_aval(int monto_aval) {
        this.monto_aval = monto_aval;
    }

    public int getMonto_acumulado() {
        return monto_acumulado;
    }

    public void setMonto_acumulado(int monto_acumulado) {
        this.monto_acumulado = monto_acumulado;
    }

    public Date getFecha_viaje() {
        return fecha_viaje;
    }

    public void setFecha_viaje(Date fecha_viaje) {
        this.fecha_viaje = fecha_viaje;
    }

    public Date getFecha_limite() {
        return fecha_limite;
    }

    public void setFecha_limite(Date fecha_limite) {
        this.fecha_limite = fecha_limite;
    }

    public byte getParticipantes() {
        return participantes;
    }

    public void setParticipantes(byte participantes) {
        this.participantes = participantes;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Empleado getEjecutivo() {
        return ejecutivo;
    }

    public void setEjecutivo(Empleado ejecutivo) {
        this.ejecutivo = ejecutivo;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    public List<Seguro> getSeguros() {
        return seguros;
    }

    public void setSeguros(List<Seguro> seguros) {
        this.seguros = seguros;
    }
    
    private static final long serialVersionUID = 1L;
}
