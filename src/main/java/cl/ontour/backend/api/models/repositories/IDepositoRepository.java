package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Deposito;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IDepositoRepository extends CrudRepository<Deposito, Long> {
    @Query("SELECT d FROM Deposito d WHERE UPPER(d.codigo) = :codigo")
    public Deposito findByCodigo(@Param("codigo") String codigo);  
}
