package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Servicio;
import cl.ontour.backend.api.models.repositories.IServicioRepository;
import cl.ontour.backend.api.models.services.IServicioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ServicioServiceImpl implements IServicioService {
    
    @Autowired
    private IServicioRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Servicio> listar() {
        return (List<Servicio>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Servicio consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Servicio consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Servicio servicio) {
        repository.save(servicio);
        if (repository.findById(servicio.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Servicio servicio) {
        Servicio nuevo_servicio = consultar(id);
        nuevo_servicio.setCodigo(servicio.getCodigo());
        nuevo_servicio.setNombre(servicio.getNombre());
        nuevo_servicio.setDetalle(servicio.getDetalle());
        nuevo_servicio.setEnlace_imagen(servicio.getEnlace_imagen());
        nuevo_servicio.setDestino(servicio.getDestino());
        nuevo_servicio.setEstado(servicio.getEstado());
        return registrar(nuevo_servicio);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Servicio servicio = consultar(id);
        servicio.setEstado(false);
        return registrar(servicio);
    }
    
    
    
}
