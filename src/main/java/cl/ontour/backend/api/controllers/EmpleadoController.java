package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.Empleado;
import cl.ontour.backend.api.models.services.IEmpleadoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/empleado")
public class EmpleadoController {
    
    @Autowired
    private IEmpleadoService service;
    
    @GetMapping
    public List<Empleado> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Empleado consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/user/{user}")
    public Empleado obtenerPorUsuario(@PathVariable(value = "user") String user) {
        return service.obtenerPorUsuario(user);
    }
    
    @PostMapping
    public String registrar(@RequestBody Empleado empleado) {
        return service.registrar(empleado);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody Empleado empleado) {
        return service.modificar(id, empleado);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}   
