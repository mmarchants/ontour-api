package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Agencia;
import cl.ontour.backend.api.models.repositories.IAgenciaRepository;
import cl.ontour.backend.api.models.services.IAgenciaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AgenciaServiceImpl implements IAgenciaService {
    
    @Autowired
    private IAgenciaRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Agencia> listar() {
        return (List<Agencia>)repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Agencia consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Agencia consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Agencia agencia) {
        repository.save(agencia);
        if (repository.findById(agencia.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Agencia agencia) {
        Agencia nueva_agencia = consultar(id);
        nueva_agencia.setCodigo(agencia.getCodigo());
        nueva_agencia.setNombre(agencia.getNombre());
        nueva_agencia.setDireccion(agencia.getDireccion());
        nueva_agencia.setTelefono(agencia.getTelefono());
        nueva_agencia.setCorreo_electronico(agencia.getCorreo_electronico());
        nueva_agencia.setEnlace_logo(agencia.getEnlace_logo());
        nueva_agencia.setCiudad(agencia.getCiudad());        
        nueva_agencia.setEstado(agencia.getEstado());        
        return registrar(nueva_agencia);        
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Agencia agencia = consultar(id);
        agencia.setEstado(false);
        return registrar(agencia);
    }
}
