package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Curso;
import cl.ontour.backend.api.models.repositories.ICursoRepository;
import cl.ontour.backend.api.models.services.ICursoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CursoServiceImpl implements ICursoService {
    
    @Autowired
    private ICursoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Curso> listar() {
        return (List<Curso>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Curso consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Curso consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Curso curso) {
        repository.save(curso);
        if (repository.findById(curso.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Curso curso) {
        Curso nuevo_curso = consultar(id);
        nuevo_curso.setCodigo(curso.getCodigo());
        nuevo_curso.setCorreo_electronico(curso.getCorreo_electronico());
        nuevo_curso.setRepresentante(curso.getRepresentante());
        nuevo_curso.setColegio(curso.getColegio());
        nuevo_curso.setEstado(curso.getEstado());
        return registrar(nuevo_curso);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Curso curso = consultar(id);
        curso.setEstado(false);
        return registrar(curso);
    }
    
    
    
}
