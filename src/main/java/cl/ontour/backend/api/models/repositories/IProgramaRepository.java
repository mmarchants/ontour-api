package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Programa;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IProgramaRepository extends CrudRepository<Programa, Long> {
    @Query("SELECT p FROM Programa p WHERE UPPER(p.codigo) = :codigo")
    public Programa findByCodigo(@Param("codigo") String codigo);  
}
