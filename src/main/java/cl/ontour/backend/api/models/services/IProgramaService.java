package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Programa;
import java.util.List;

public interface IProgramaService {
    
    public List<Programa> listar();
    public Programa consultar(Long id);    
    public String registrar(Programa programa);    
    public String modificar(Long id, Programa programa);    
    public String eliminar(Long id);
    public Programa consultarCodigo(String codigo);
    
}
