package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Traslado;
import java.util.List;

public interface ITrasladoService {
    
    public List<Traslado> listar();
    public Traslado consultar(Long id);    
    public String registrar(Traslado traslado);    
    public String modificar(Long id, Traslado traslado);    
    public String eliminar(Long id);
    public Traslado consultarCodigo(String codigo);
}
