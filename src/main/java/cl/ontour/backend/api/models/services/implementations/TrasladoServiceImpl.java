package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Traslado;
import cl.ontour.backend.api.models.repositories.ITrasladoRepository;
import cl.ontour.backend.api.models.services.ITrasladoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TrasladoServiceImpl implements ITrasladoService {
    
    @Autowired
    private ITrasladoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Traslado> listar() {
        return (List<Traslado>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Traslado consultar(Long id) {
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(Traslado traslado) {
        repository.save(traslado);
        if (repository.findById(traslado.getId()) != null) {
            return "Ok";            
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Traslado traslado) {
        Traslado nuevo_traslado = consultar(id);
        nuevo_traslado.setCodigo(traslado.getCodigo());
        nuevo_traslado.setNombre(traslado.getNombre());
        nuevo_traslado.setDetalle(traslado.getDetalle());
        nuevo_traslado.setFecha_partida(traslado.getFecha_partida());
        nuevo_traslado.setFecha_llegada(traslado.getFecha_llegada());
        nuevo_traslado.setOrigen(traslado.getOrigen());
        nuevo_traslado.setDestino(traslado.getDestino());
        nuevo_traslado.setTipo_traslado(traslado.getTipo_traslado());
        nuevo_traslado.setEstado(traslado.getEstado());
        return registrar(nuevo_traslado);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Traslado traslado = consultar(id);
        traslado.setEstado(false);
        return registrar(traslado);
    }

    @Override
    @Transactional(readOnly = true)
    public Traslado consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }
}
