package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.TipoContrato;
import cl.ontour.backend.api.models.repositories.ITipoContratoRepository;
import cl.ontour.backend.api.models.services.ITipoContratoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TipoContratoServiceImpl implements ITipoContratoService {
    
    @Autowired
    private ITipoContratoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<TipoContrato> listar() {
        return (List<TipoContrato>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public TipoContrato consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public TipoContrato consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(TipoContrato tipo_contrato) {
        repository.save(tipo_contrato);
        if (repository.findById(tipo_contrato.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, TipoContrato tipo_contrato) {
        TipoContrato nuevo_tipo_contrato = consultar(id);
        nuevo_tipo_contrato.setCodigo(tipo_contrato.getCodigo());
        nuevo_tipo_contrato.setNombre(tipo_contrato.getNombre());
        nuevo_tipo_contrato.setDescripcion(tipo_contrato.getDescripcion());
        nuevo_tipo_contrato.setEstado(tipo_contrato.getEstado());
        return registrar(nuevo_tipo_contrato);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        TipoContrato tipo_contrato = consultar(id);
        tipo_contrato.setEstado(false);
        return registrar(tipo_contrato);
    }
}
