package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.TipoEvento;
import java.util.List;

public interface ITipoEventoService {
    
    public List<TipoEvento> listar();
    public TipoEvento consultar(Long id);    
    public String registrar(TipoEvento tipo_evento);    
    public String modificar(Long id, TipoEvento tipo_evento);    
    public String eliminar(Long id);
    public TipoEvento consultarCodigo(String codigo);
}
