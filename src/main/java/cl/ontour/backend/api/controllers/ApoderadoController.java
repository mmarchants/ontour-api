package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.Apoderado;
import cl.ontour.backend.api.models.services.IApoderadoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/apoderado")
public class ApoderadoController {
    
    @Autowired
    private IApoderadoService service;
    
    @GetMapping
    public List<Apoderado> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Apoderado consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/user/{user}")
    public Apoderado obtenerPorUsuario(@PathVariable(value = "user") String user) {
        return service.obtenerPorUsuario(user);
    }
    
    @PostMapping
    public String registrar(@RequestBody Apoderado apoderado) {
        return service.registrar(apoderado);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody Apoderado apoderado) {
        return service.modificar(id, apoderado);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}