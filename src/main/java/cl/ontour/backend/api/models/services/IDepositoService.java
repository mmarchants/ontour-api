package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Deposito;
import java.util.List;

public interface IDepositoService {
    
    public List<Deposito> listar();
    public Deposito consultar(Long id);    
    public String registrar(Deposito deposito);    
    public String modificar(Long id, Deposito deposito);    
    public String eliminar(Long id);
    public Deposito consultarCodigo(String codigo);
}
