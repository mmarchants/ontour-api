package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Colegio;
import java.util.List;

public interface IColegioService {
    
    public List<Colegio> listar();
    public Colegio consultar(Long id);    
    public String registrar(Colegio colegio);    
    public String modificar(Long id, Colegio colegio);    
    public String eliminar(Long id);
    public Colegio consultarCodigo(String codigo);    
}
