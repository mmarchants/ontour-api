package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Usuario;
import cl.ontour.backend.api.models.repositories.IApoderadoRepository;
import cl.ontour.backend.api.models.repositories.IEmpleadoRepository;
import cl.ontour.backend.api.models.repositories.IUsuarioRepository;
import cl.ontour.backend.api.models.services.IUsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioServiceImpl implements IUsuarioService {
    
    @Autowired
    private IUsuarioRepository repository;
    
    @Autowired IApoderadoRepository apoderado;
    
    @Autowired IEmpleadoRepository empleado;

    @Override
    @Transactional(readOnly = true)
    public List<Usuario> listar() {
        return (List<Usuario>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Usuario consultar(Long id) {        
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(Usuario usuario) {
        usuario.setContrasegna(new BCryptPasswordEncoder().encode(usuario.getContrasegna()));
        repository.save(usuario);
        if (repository.findById(usuario.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }
    
    @Override
    @Transactional
    public String modificar(Long id, Usuario usuario) {
        Usuario nuevo_usuario = consultar(id);
        nuevo_usuario.setCorreo_electronico(usuario.getCorreo_electronico());        
        nuevo_usuario.setEstado(usuario.getEstado());
        nuevo_usuario.setRol(usuario.getRol());
        if (usuario.getContrasegna().isEmpty()) {
            repository.save(nuevo_usuario);
            if (repository.findById(nuevo_usuario.getId()) != null) {
                return "Ok";
            }
            return "Error";
        } else {
            nuevo_usuario.setContrasegna(usuario.getContrasegna());
        }
        return registrar(nuevo_usuario);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Usuario usuario = consultar(id);
        usuario.setEstado(false);
        return registrar(usuario);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Usuario obtenerPorCorreo(String correo_electronico) {
        return repository.findByCorreoElectronico(correo_electronico);
    }
}
