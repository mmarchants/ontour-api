package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Programa;
import cl.ontour.backend.api.models.repositories.IProgramaRepository;
import cl.ontour.backend.api.models.services.IProgramaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProgramaServiceImpl implements IProgramaService {
    
    @Autowired
    private IProgramaRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Programa> listar() {
        return (List<Programa>)repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Programa consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Programa consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Programa programa) {
        repository.save(programa);
        if (repository.findById(programa.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Programa programa) {
        Programa nuevo_programa = consultar(id);
        nuevo_programa.setCodigo(programa.getCodigo());
        nuevo_programa.setNombre(programa.getNombre());
        nuevo_programa.setDetalle(programa.getDetalle());
        nuevo_programa.setValor(programa.getValor());
        nuevo_programa.setEstado(programa.getEstado());
        nuevo_programa.setHospedajes(programa.getHospedajes());
        nuevo_programa.setTraslados(programa.getTraslados());
        return registrar(nuevo_programa);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Programa programa = consultar(id);
        programa.setEstado(false);
        return registrar(programa);
    }
    
}
