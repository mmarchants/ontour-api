package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Curso;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ICursoRepository extends CrudRepository<Curso, Long> {    
    @Query("SELECT c FROM Curso c WHERE UPPER(c.codigo) = :codigo")
    public Curso findByCodigo(@Param("codigo") String codigo);  
}
