package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Curso;
import java.util.List;

public interface ICursoService {
    
    public List<Curso> listar();
    public Curso consultar(Long id);    
    public String registrar(Curso curso);    
    public String modificar(Long id, Curso curso);    
    public String eliminar(Long id);
    public Curso consultarCodigo(String codigo);
}
