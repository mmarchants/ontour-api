package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.Servicio;
import cl.ontour.backend.api.models.services.IServicioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/servicio")
public class ServicioController {
    
    @Autowired
    private IServicioService service;
    
    @GetMapping
    public List<Servicio> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Servicio consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/codigo/{codigo}")
    public Servicio consultarCodigo(@PathVariable(value = "codigo") String codigo) {
        return service.consultarCodigo(codigo);
    }
    
    @PostMapping
    public String registrar(@RequestBody Servicio servicio) {
        return service.registrar(servicio);
    }

    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody Servicio servicio) {
        return service.modificar(id, servicio);        
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
