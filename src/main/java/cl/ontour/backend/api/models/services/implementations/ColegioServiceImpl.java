package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Colegio;
import cl.ontour.backend.api.models.repositories.IColegioRepository;
import cl.ontour.backend.api.models.services.IColegioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ColegioServiceImpl implements IColegioService {
    
    @Autowired
    private IColegioRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Colegio> listar() {
        return (List<Colegio>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Colegio consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Colegio consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Colegio colegio) {
        repository.save(colegio);
        if (repository.findById(colegio.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Colegio colegio) {
        Colegio nuevo_colegio = consultar(id);
        nuevo_colegio.setCodigo(colegio.getCodigo());
        nuevo_colegio.setNombre(colegio.getNombre());
        nuevo_colegio.setDireccion(colegio.getDireccion());
        nuevo_colegio.setTelefono(colegio.getTelefono());
        nuevo_colegio.setCorreo_electronico(colegio.getCorreo_electronico());
        nuevo_colegio.setCiudad(colegio.getCiudad());
        nuevo_colegio.setEstado(colegio.getEstado());
        return registrar(nuevo_colegio);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Colegio colegio = consultar(id);
        colegio.setEstado(false);
        return registrar(colegio);
    }
    
    
    
}
