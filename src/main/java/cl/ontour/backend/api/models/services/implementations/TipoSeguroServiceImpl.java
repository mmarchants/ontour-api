package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.TipoSeguro;
import cl.ontour.backend.api.models.repositories.ITipoSeguroRepository;
import cl.ontour.backend.api.models.services.ITipoSeguroService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TipoSeguroServiceImpl implements ITipoSeguroService {
    
    @Autowired
    private ITipoSeguroRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<TipoSeguro> listar() {
        return (List<TipoSeguro>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public TipoSeguro consultar(Long id) {
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(TipoSeguro tipo_seguro) {
        repository.save(tipo_seguro);
        if (repository.findById(tipo_seguro.getId()) != null ) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, TipoSeguro tipo_seguro) {
        TipoSeguro nuevo_tipo_seguro = consultar(id);
        nuevo_tipo_seguro.setCodigo(tipo_seguro.getCodigo());
        nuevo_tipo_seguro.setNombre(tipo_seguro.getNombre());
        nuevo_tipo_seguro.setEstado(tipo_seguro.getEstado());
        return registrar(nuevo_tipo_seguro);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        TipoSeguro tipo_seguro = consultar(id);
        tipo_seguro.setEstado(false);
        return registrar(tipo_seguro);
    }

    @Override
    @Transactional(readOnly = true)
    public TipoSeguro consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }
}
