package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.TipoContrato;
import java.util.List;

public interface ITipoContratoService {
    
    public List<TipoContrato> listar();
    public TipoContrato consultar(Long id);    
    public String registrar(TipoContrato tipo_contrato);    
    public String modificar(Long id, TipoContrato tipo_contrato);    
    public String eliminar(Long id);
    public TipoContrato consultarCodigo(String codigo);
}
