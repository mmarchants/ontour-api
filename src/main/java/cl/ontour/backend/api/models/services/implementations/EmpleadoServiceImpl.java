package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Empleado;
import cl.ontour.backend.api.models.repositories.IEmpleadoRepository;
import cl.ontour.backend.api.models.services.IEmpleadoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService {
    
    @Autowired
    private IEmpleadoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Empleado> listar() {
        return (List<Empleado>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Empleado consultar(Long id) {
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(Empleado empleado) {
        repository.save(empleado);
        if (repository.findById(empleado.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Empleado empleado) {
        Empleado nuevo_empleado = consultar(id);
        nuevo_empleado.setCodigo(empleado.getCodigo());
        nuevo_empleado.setNombre(empleado.getNombre());
        nuevo_empleado.setPrimer_apellido(empleado.getPrimer_apellido());
        nuevo_empleado.setSegundo_apellido(empleado.getSegundo_apellido());
        nuevo_empleado.setTelefono(empleado.getTelefono());
        nuevo_empleado.setUsuario(empleado.getUsuario());
        nuevo_empleado.setAgencia(empleado.getAgencia());
        return registrar(nuevo_empleado);        
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Empleado empleado = consultar(id);
        empleado.getUsuario().setEstado(false);
        return registrar(empleado);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Empleado obtenerPorUsuario(String correo_electronico) {
        return repository.findByUsuario(correo_electronico);
    }
}
