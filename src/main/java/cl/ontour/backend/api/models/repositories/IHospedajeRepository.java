package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Hospedaje;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IHospedajeRepository extends CrudRepository<Hospedaje, Long>{

    @Query("SELECT h FROM Hospedaje h WHERE UPPER(h.codigo) = :codigo")
    public Hospedaje findByCodigo(@Param("codigo") String codigo);  
}
