package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.TipoTraslado;
import java.util.List;

public interface ITipoTrasladoService {
    
    public List<TipoTraslado> listar();
    public TipoTraslado consultar(Long id);    
    public String registrar(TipoTraslado tipo_traslado);    
    public String modificar(Long id, TipoTraslado tipo_traslado);    
    public String eliminar(Long id);
    public TipoTraslado consultarCodigo(String codigo);
}
