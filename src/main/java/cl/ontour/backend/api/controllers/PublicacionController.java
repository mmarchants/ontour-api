package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.Publicacion;
import cl.ontour.backend.api.models.services.IPublicacionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/publicacion")
public class PublicacionController {
    
    @Autowired
    private IPublicacionService service;
    
    @GetMapping
    public List<Publicacion> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Publicacion consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @PostMapping
    public String registrar(@RequestBody Publicacion publicacion) {
        return service.registrar(publicacion);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody Publicacion publicacion) {
        return service.modificar(id, publicacion);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
