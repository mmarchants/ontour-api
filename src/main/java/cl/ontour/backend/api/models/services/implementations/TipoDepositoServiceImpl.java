package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.TipoDeposito;
import cl.ontour.backend.api.models.repositories.ITipoDepositoRepository;
import cl.ontour.backend.api.models.services.ITipoDepositoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TipoDepositoServiceImpl implements ITipoDepositoService {
    
    @Autowired
    private ITipoDepositoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<TipoDeposito> listar() {
        return (List<TipoDeposito>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public TipoDeposito consultar(Long id) {
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(TipoDeposito tipo_deposito) {
        repository.save(tipo_deposito);
        if (repository.findById(tipo_deposito.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, TipoDeposito tipo_deposito) {
        TipoDeposito nuevo_tipo_deposito = consultar(id);
        nuevo_tipo_deposito.setCodigo(tipo_deposito.getCodigo());
        nuevo_tipo_deposito.setNombre(tipo_deposito.getNombre());
        nuevo_tipo_deposito.setEstado(tipo_deposito.getEstado());
        return registrar(nuevo_tipo_deposito);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        TipoDeposito tipo_deposito = consultar(id);
        tipo_deposito.setEstado(false);
        return registrar(tipo_deposito);
    }

    @Override
    @Transactional(readOnly = true)
    public TipoDeposito consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }
}
