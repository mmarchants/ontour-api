package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Alumno;
import org.springframework.data.repository.CrudRepository;

public interface IAlumnoRepository extends CrudRepository<Alumno, Long> {
    
}
