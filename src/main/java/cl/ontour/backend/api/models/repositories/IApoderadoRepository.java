package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Apoderado;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IApoderadoRepository extends CrudRepository<Apoderado, Long> {
    
    @Query("SELECT a FROM Apoderado a WHERE a.usuario.correo_electronico = :correo")
    public Apoderado findByUsuario(@Param("correo") String correo_electronico);
}
