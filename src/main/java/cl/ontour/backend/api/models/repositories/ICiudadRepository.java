package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Ciudad;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ICiudadRepository extends CrudRepository<Ciudad, Long> {
    
    @Query("SELECT c FROM Ciudad c WHERE UPPER(c.codigo) = :codigo")
    public Ciudad findByCodigo(@Param("codigo") String codigo);
    
}
