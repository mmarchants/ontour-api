package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.TipoSeguro;
import cl.ontour.backend.api.models.services.ITipoSeguroService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/tipo_seguro")
public class TipoSeguroController {
    
    @Autowired
    private ITipoSeguroService service;
    
    @GetMapping
    public List<TipoSeguro> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public TipoSeguro consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/codigo/{codigo}")
    public TipoSeguro consultarCodigo(@PathVariable(value = "codigo") String codigo) {
        return service.consultarCodigo(codigo);
    }
    
    @PostMapping
    public String registrar(@RequestBody TipoSeguro tipo_seguro) {
        return service.registrar(tipo_seguro);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody TipoSeguro tipo_seguro) {
        return service.modificar(id, tipo_seguro);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
