package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Seguro;
import cl.ontour.backend.api.models.repositories.ISeguroRepository;
import cl.ontour.backend.api.models.services.ISeguroService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SeguroServiceImpl implements ISeguroService {
    
    @Autowired
    private ISeguroRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Seguro> listar() {
        return (List<Seguro>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Seguro consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Seguro consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Seguro seguro) {
        repository.save(seguro);
        if (repository.findById(seguro.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Seguro seguro) {
        Seguro nuevo_seguro = consultar(id);
        nuevo_seguro.setCodigo(seguro.getCodigo());
        nuevo_seguro.setNombre(seguro.getNombre());
        nuevo_seguro.setDetalle(seguro.getDetalle());
        nuevo_seguro.setCobertura(seguro.getCobertura());
        nuevo_seguro.setValor(seguro.getValor());
        nuevo_seguro.setFecha_inicio(seguro.getFecha_inicio());
        nuevo_seguro.setFecha_termino(seguro.getFecha_termino());
        nuevo_seguro.setTipo_seguro(seguro.getTipo_seguro());
        nuevo_seguro.setEstado(seguro.getEstado());
        return registrar(nuevo_seguro);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Seguro seguro = consultar(id);
        seguro.setEstado(false);
        return registrar(seguro);
    }
}
