package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Alumno;
import cl.ontour.backend.api.models.repositories.IAlumnoRepository;
import cl.ontour.backend.api.models.services.IAlumnoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AlumnoServiceImpl implements IAlumnoService {
    
    @Autowired
    private IAlumnoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Alumno> listar() {
        return (List<Alumno>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Alumno consultar(Long id) {
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(Alumno alumno) {
        repository.save(alumno);
        if (repository.findById(alumno.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Alumno alumno) {        
        Alumno nuevo_alumno = consultar(id);
        nuevo_alumno.setDni(alumno.getDni());
        nuevo_alumno.setNombres(alumno.getNombres());
        nuevo_alumno.setPrimer_apellido(alumno.getPrimer_apellido());
        nuevo_alumno.setSegundo_apellido(alumno.getSegundo_apellido());
        nuevo_alumno.setCorreo_electronico(alumno.getCorreo_electronico());
        nuevo_alumno.setCurso(alumno.getCurso());
        nuevo_alumno.setApoderado(alumno.getApoderado());
        nuevo_alumno.setEstado(alumno.getEstado());
        return registrar(nuevo_alumno);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Alumno alumno = consultar(id);
        alumno.setEstado(false);
        return registrar(alumno);
    }
}
