package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.Reserva;
import cl.ontour.backend.api.models.services.IReservaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/reserva")
public class ReservaController {
    
    @Autowired
    private IReservaService service;
    
    @GetMapping
    public List<Reserva> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Reserva consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/codigo/{codigo}")
    public Reserva consultarCodigo(@PathVariable(value = "codigo") String codigo) {
        return service.consultarCodigo(codigo);
    }
    
    @PostMapping
    public String registrar(@RequestBody Reserva reserva) {
        return service.registrar(reserva);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody Reserva reserva) {
        return service.modificar(id, reserva);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
