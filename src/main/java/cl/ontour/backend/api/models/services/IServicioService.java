package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Servicio;
import java.util.List;

public interface IServicioService {
    
    public List<Servicio> listar();
    public Servicio consultar(Long id);    
    public String registrar(Servicio servicio);    
    public String modificar(Long id, Servicio servicio);    
    public String eliminar(Long id);
    public Servicio consultarCodigo(String codigo);
}
