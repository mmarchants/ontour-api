package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Ciudad;
import java.util.List;

public interface ICiudadService {
    
    public List<Ciudad> listar();
    public Ciudad consultar(Long id);    
    public String registrar(Ciudad ciudad);    
    public String modificar(Long id, Ciudad ciudad);    
    public String eliminar(Long id);
    public Ciudad consultarCodigo(String codigo);
    
}
