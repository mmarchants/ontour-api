package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Destino;
import java.util.List;

public interface IDestinoService {
    
    public List<Destino> listar();
    public Destino consultar(Long id);    
    public String registrar(Destino destino);    
    public String modificar(Long id, Destino destino);    
    public String eliminar(Long id);
    public Destino consultarCodigo(String codigo);
    
}
