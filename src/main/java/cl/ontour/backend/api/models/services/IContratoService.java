package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Contrato;
import java.util.List;

public interface IContratoService {
    
    public List<Contrato> listar();
    public Contrato consultar(Long id);    
    public String registrar(Contrato contrato);    
    public String modificar(Long id, Contrato contrato);    
    public String eliminar(Long id);
    public Contrato consultarCodigo(String codigo);
}
