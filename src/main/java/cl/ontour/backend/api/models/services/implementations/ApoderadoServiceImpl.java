package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Apoderado;
import cl.ontour.backend.api.models.repositories.IApoderadoRepository;
import cl.ontour.backend.api.models.services.IApoderadoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ApoderadoServiceImpl implements IApoderadoService {
    
    @Autowired
    private IApoderadoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Apoderado> listar() {
        return (List<Apoderado>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Apoderado consultar(Long id) {
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(Apoderado apoderado) {
        repository.save(apoderado);
        if (repository.findById(apoderado.getId()) != null) {
            return "Ok";            
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Apoderado apoderado) {
        Apoderado nuevo_apoderado = consultar(id);
        nuevo_apoderado.setNombre(apoderado.getNombre());
        nuevo_apoderado.setPrimer_apellido(apoderado.getPrimer_apellido());
        nuevo_apoderado.setSegundo_apellido(apoderado.getSegundo_apellido());
        nuevo_apoderado.setTelefono(apoderado.getTelefono());
        nuevo_apoderado.setUsuario(apoderado.getUsuario());
        return registrar(nuevo_apoderado);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Apoderado apoderado = consultar(id);
        apoderado.getUsuario().setEstado(false);
        return registrar(apoderado);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Apoderado obtenerPorUsuario(String correo_electronico) {
        return repository.findByUsuario(correo_electronico);
    }
}
