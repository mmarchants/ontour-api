package cl.ontour.backend.api.models.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Publicacion implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_publicacion")
    private Long id;
    private String mensaje;
    private String enlace_documento;
    @ManyToOne
    @JoinColumn(name = "id_ejecutivo", referencedColumnName = "id_empleado")
    private Empleado ejecutivo;
    @ManyToOne
    @JoinColumn(name = "id_reserva", referencedColumnName = "id_reserva")
    private Reserva reserva;
    private boolean estado;    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getEnlace_documento() {
        return enlace_documento;
    }

    public void setEnlace_documento(String enlace_documento) {
        this.enlace_documento = enlace_documento;
    }

    public Empleado getEjecutivo() {
        return ejecutivo;
    }

    public void setEjecutivo(Empleado ejecutivo) {
        this.ejecutivo = ejecutivo;
    }

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    private static final long serialVersionUID = 1L;
}
