package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Servicio;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IServicioRepository extends CrudRepository<Servicio, Long>{
    
    @Query("SELECT s FROM Servicio s WHERE UPPER(s.codigo) = :codigo")
    public Servicio findByCodigo(@Param("codigo") String codigo);  
    
}
