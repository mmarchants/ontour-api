package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.TipoDeposito;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ITipoDepositoRepository extends CrudRepository<TipoDeposito, Long> {
    @Query("SELECT t FROM TipoDeposito t WHERE UPPER(t.codigo) = :codigo")
    public TipoDeposito findByCodigo(@Param("codigo") String codigo);  
}
