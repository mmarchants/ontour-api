package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.TipoTraslado;
import cl.ontour.backend.api.models.services.ITipoTrasladoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/tipo_traslado")
public class TipoTrasladoController {
    
    @Autowired
    private ITipoTrasladoService service;
    
    @GetMapping
    public List<TipoTraslado> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public TipoTraslado consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/codigo/{codigo}")
    public TipoTraslado consultarCodigo(@PathVariable(value = "codigo") String codigo) {
        return service.consultarCodigo(codigo);
    }
    
    @PostMapping
    public String registrar(@RequestBody TipoTraslado tipo_traslado) {
        return service.registrar(tipo_traslado);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody TipoTraslado tipo_traslado) {
        return service.modificar(id, tipo_traslado);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
