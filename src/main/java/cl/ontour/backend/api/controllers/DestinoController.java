package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.Destino;
import cl.ontour.backend.api.models.services.IDestinoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/destino")
public class DestinoController {
    
    @Autowired
    private IDestinoService service;
    
    @GetMapping
    public List<Destino> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Destino consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("codigo/{codigo}")
    public Destino consultarCodigo(@PathVariable(value = "codigo") String codigo) {
        return service.consultarCodigo(codigo);
    }
    
    @PostMapping
    public String registrar(@RequestBody Destino destino) {
        return service.registrar(destino);
    }

    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody Destino destino) {
        return service.modificar(id, destino);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
