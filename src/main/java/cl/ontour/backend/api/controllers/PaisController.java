package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.Pais;
import cl.ontour.backend.api.models.services.IPaisService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/pais")
public class PaisController {
    
    @Autowired
    private IPaisService service;
    
    @GetMapping
    public List<Pais> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Pais consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/codigo/{codigo}")
    public Pais consultarCodigo(@PathVariable(value = "codigo") String codigo) {
        return service.consultarCodigo(codigo);
    }
    
    @PostMapping
    public String registrar(@RequestBody Pais pais) {
        return service.registrar(pais);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody Pais pais) {
        return service.modificar(id, pais);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
