package cl.ontour.backend.api.auth.filter;

import cl.ontour.backend.api.auth.service.JWTService;
import cl.ontour.backend.api.auth.service.implementations.JWTServiceImpl;
import cl.ontour.backend.api.models.entities.Rol;
import cl.ontour.backend.api.models.entities.Usuario;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    
    private JWTService jwtService;
    
    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, JWTService jwtService) {
        this.authenticationManager = authenticationManager;
        this.jwtService = jwtService;
    }
    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws org.springframework.security.core.AuthenticationException {
        String username = obtainUsername(request);
        String password = obtainPassword(request);        
        if (username != null && password != null) {
            logger.info("User [form-data]: " + username);
            logger.info("Password [form-data]: " + password);
        } else {
            Usuario user = null;
            try {
                user = new ObjectMapper().readValue(request.getInputStream(), Usuario.class);
                username = user.getCorreo_electronico();
                password = user.getContrasegna();
                logger.info("User [raw]: " + username);
                logger.info("Password [raw]: " + password);
            } catch (IOException ex) {
                Logger.getLogger(JWTAuthenticationFilter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        username = username.trim();
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, password);    
        return authenticationManager.authenticate(authToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {        
        String token = jwtService.create(authResult);       
        
        Usuario user = new Usuario();
        user.setCorreo_electronico(authResult.getName());        
        Rol rol = new Rol();
        String codigo = authResult.getAuthorities().toString();
        rol.setCodigo(codigo.substring(1, codigo.length()-1));        
        user.setRol(rol);
        
        response.addHeader(JWTServiceImpl.HEADER_STRING, JWTServiceImpl.TOKEN_PREFIX + token);
        Map<String,Object> body = new HashMap<String, Object>();
        body.put("token", token);
        body.put("usuario", user);
        body.put("mensaje", "Autenticación exitosa");
        response.getWriter().write(new ObjectMapper().writeValueAsString(body));
        response.setStatus(200);
        response.setContentType("application/json");
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        Map<String, Object> body = new HashMap<String, Object>();
        body.put("mensaje", "Credenciales incorrectas");
        body.put("error", failed.getMessage());
        response.getWriter().write(new ObjectMapper().writeValueAsString(body));
        response.setStatus(401);
        response.setContentType("application/json");
    }
}
