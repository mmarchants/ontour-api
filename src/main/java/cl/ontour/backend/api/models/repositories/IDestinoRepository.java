package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Destino;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IDestinoRepository extends CrudRepository<Destino, Long> {
    
    @Query("SELECT d FROM Destino d WHERE UPPER(d.codigo) = :codigo")
    public Destino findByCodigo(@Param("codigo") String codigo);
    
}
