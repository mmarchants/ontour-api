package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Reserva;
import cl.ontour.backend.api.models.repositories.IReservaRepository;
import cl.ontour.backend.api.models.services.IReservaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ReservaServiceImpl implements IReservaService {
    
    @Autowired
    private IReservaRepository repository;

    @Override
    @Transactional
    public List<Reserva> listar() {
        return (List<Reserva>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Reserva consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Reserva consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Reserva reserva) {
        repository.save(reserva);
        if (repository.findById(reserva.getId()) != null) {
            return "Ok";            
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Reserva reserva) {
        Reserva nueva_reserva = consultar(id);
        nueva_reserva.setCodigo(reserva.getCodigo());
        nueva_reserva.setDetalle(reserva.getDetalle());
        nueva_reserva.setValor(reserva.getValor());
        nueva_reserva.setMonto_aval(reserva.getMonto_aval());
        nueva_reserva.setMonto_acumulado(reserva.getMonto_acumulado());
        nueva_reserva.setFecha_viaje(reserva.getFecha_viaje());
        nueva_reserva.setFecha_limite(reserva.getFecha_limite());
        nueva_reserva.setParticipantes(reserva.getParticipantes());
        nueva_reserva.setContrato(reserva.getContrato());
        nueva_reserva.setEjecutivo(reserva.getEjecutivo());
        nueva_reserva.setCurso(reserva.getCurso());
        nueva_reserva.setPrograma(reserva.getPrograma());
        nueva_reserva.setEstado(reserva.getEstado());
        nueva_reserva.setSeguros(reserva.getSeguros());
        return registrar(nueva_reserva);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Reserva reserva = consultar(id);
        reserva.setEstado(false);
        return registrar(reserva);
    }
    
}
