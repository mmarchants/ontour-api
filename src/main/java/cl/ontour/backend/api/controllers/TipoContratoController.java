package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.TipoContrato;
import cl.ontour.backend.api.models.services.ITipoContratoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/tipo_contrato")
public class TipoContratoController {
    
    @Autowired
    public ITipoContratoService service;

    @GetMapping
    public List<TipoContrato> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public TipoContrato consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/codigo/{codigo}")
    public TipoContrato consultarCodigo(@PathVariable(value = "codigo") String codigo) {
        return service.consultarCodigo(codigo);
    }
    
    @PostMapping
    public String registrar(@RequestBody TipoContrato tipo_contrato) {
        return service.registrar(tipo_contrato);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody TipoContrato tipo_contrato) {
        return service.modificar(id, tipo_contrato);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
