package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Apoderado;
import java.util.List;

public interface IApoderadoService {
    
    public List<Apoderado> listar();
    public Apoderado consultar(Long id);    
    public String registrar(Apoderado apoderado);    
    public String modificar(Long id, Apoderado apoderado);    
    public String eliminar(Long id);
    public Apoderado obtenerPorUsuario(String correo_electronico);
    
}
