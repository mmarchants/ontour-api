package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Publicacion;
import cl.ontour.backend.api.models.repositories.IPublicacionRepository;
import cl.ontour.backend.api.models.services.IPublicacionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PublicacionServiceImpl implements IPublicacionService {
    
    @Autowired
    private IPublicacionRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Publicacion> listar() {
        return (List<Publicacion>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Publicacion consultar(Long id) {
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(Publicacion publicacion) {
        repository.save(publicacion);
        if (repository.findById(publicacion.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Publicacion publicacion) {
        Publicacion nueva_publicacion = consultar(id);
        nueva_publicacion.setMensaje(publicacion.getMensaje());
        nueva_publicacion.setEnlace_documento(publicacion.getEnlace_documento());
        nueva_publicacion.setEjecutivo(publicacion.getEjecutivo());
        nueva_publicacion.setReserva(publicacion.getReserva());
        nueva_publicacion.setEstado(publicacion.getEstado());
        return registrar(nueva_publicacion);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Publicacion publicacion = consultar(id);
        publicacion.setEstado(false);
        return registrar(publicacion);
    }    
}
