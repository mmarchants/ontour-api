package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Seguro;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ISeguroRepository extends CrudRepository<Seguro, Long> {
    
    @Query("SELECT s FROM Seguro s WHERE UPPER(s.codigo) = :codigo")
    public Seguro findByCodigo(@Param("codigo") String codigo);
}
