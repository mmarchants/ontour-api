package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Deposito;
import cl.ontour.backend.api.models.repositories.IDepositoRepository;
import cl.ontour.backend.api.models.services.IDepositoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DepositoServiceImpl implements IDepositoService {
    
    @Autowired
    private IDepositoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Deposito> listar() {
        return (List<Deposito>) repository.findAll();        
    }

    @Override
    @Transactional(readOnly = true)
    public Deposito consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Deposito consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Deposito deposito) {
        repository.save(deposito);
        if (repository.findById(deposito.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Deposito deposito) {
        Deposito nuevo_deposito = consultar(id);
        nuevo_deposito.setCodigo(deposito.getCodigo());
        nuevo_deposito.setFecha_emision(deposito.getFecha_emision());
        nuevo_deposito.setMonto(deposito.getMonto());
        nuevo_deposito.setDetalle(deposito.getDetalle());
        nuevo_deposito.setAlumno(deposito.getAlumno());
        nuevo_deposito.setReserva(deposito.getReserva());
        nuevo_deposito.setTipo_deposito(deposito.getTipo_deposito());
        nuevo_deposito.setEstado(deposito.getEstado());
        return registrar(nuevo_deposito);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Deposito deposito = consultar(id);
        deposito.setEstado(false);
        return registrar(deposito);
    }
}
