package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Reserva;
import java.util.List;

public interface IReservaService {
    
    public List<Reserva> listar();
    public Reserva consultar(Long id);    
    public String registrar(Reserva reserva);    
    public String modificar(Long id, Reserva reserva);    
    public String eliminar(Long id);
    public Reserva consultarCodigo(String codigo);
}
