package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.TipoTraslado;
import cl.ontour.backend.api.models.repositories.ITipoTrasladoRepository;
import cl.ontour.backend.api.models.services.ITipoTrasladoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TipoTrasladoServiceImpl implements ITipoTrasladoService {
    
    @Autowired
    private ITipoTrasladoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<TipoTraslado> listar() {
        return (List<TipoTraslado>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public TipoTraslado consultar(Long id) {
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(TipoTraslado tipo_traslado) {
        repository.save(tipo_traslado);
        if (repository.findById(tipo_traslado.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, TipoTraslado tipo_traslado) {
        TipoTraslado nuevo_tipo_traslado = consultar(id);
        nuevo_tipo_traslado.setCodigo(tipo_traslado.getCodigo());
        nuevo_tipo_traslado.setNombre(tipo_traslado.getNombre());
        nuevo_tipo_traslado.setDescripcion(tipo_traslado.getDescripcion());
        nuevo_tipo_traslado.setEstado(tipo_traslado.getEstado());
        return registrar(nuevo_tipo_traslado);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        TipoTraslado tipo_traslado = consultar(id);
        tipo_traslado.setEstado(false);
        return registrar(tipo_traslado);
    }

    @Override
    @Transactional(readOnly = true)
    public TipoTraslado consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }
}
