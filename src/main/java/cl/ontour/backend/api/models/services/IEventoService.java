package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Evento;
import java.util.List;

public interface IEventoService {
    
    public List<Evento> listar();
    public Evento consultar(Long id);    
    public String registrar(Evento evento);    
    public String modificar(Long id, Evento evento);    
    public String eliminar(Long id);
    public Evento consultarCodigo(String codigo);
}
