package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.TipoEvento;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ITipoEventoRepository extends CrudRepository<TipoEvento, Long> {
    @Query("SELECT t FROM TipoEvento t WHERE UPPER(t.codigo) = :codigo")
    public TipoEvento findByCodigo(@Param("codigo") String codigo);  
}
