package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Usuario;
import cl.ontour.backend.api.models.repositories.IUsuarioRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class JpaUserDetailsService implements UserDetailsService {
    
    @Autowired
    private IUsuarioRepository repository;

    private Logger logger = LoggerFactory.getLogger(JpaUserDetailsService.class);
    
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String correo_electronico) throws UsernameNotFoundException {
        Usuario usuario = repository.findByCorreoElectronico(correo_electronico);
        if (usuario == null) {
            logger.error("Login error: No existe usuario '" + correo_electronico + "'.");
            throw new UsernameNotFoundException("No existe usuario '" + correo_electronico + "'.");
        }
        List<GrantedAuthority> roles = new ArrayList<>();
        // Para agregar más roles, modificar relación en entidad y realizar for-each.        
        roles.add(new SimpleGrantedAuthority(usuario.getRol().getCodigo()));
        logger.info("Rol: ".concat(usuario.getRol().getCodigo()));
        if (roles.isEmpty()) {
            logger.error("Login error: Usuario '" + correo_electronico + "' no tiene rol asignado.");
            throw new UsernameNotFoundException("Usuario '" + correo_electronico + "' no tiene rol asignado.");
        }
        return new User(usuario.getCorreo_electronico(), usuario.getContrasegna(), usuario.getEstado(), true, true, true, roles);
    }
    
}
