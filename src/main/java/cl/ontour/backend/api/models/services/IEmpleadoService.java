package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Empleado;
import java.util.List;

public interface IEmpleadoService {
    
    public List<Empleado> listar();
    public Empleado consultar(Long id);    
    public String registrar(Empleado empleado);    
    public String modificar(Long id, Empleado empleado);    
    public String eliminar(Long id);
    public Empleado obtenerPorUsuario(String correo_electronico);
    
}
