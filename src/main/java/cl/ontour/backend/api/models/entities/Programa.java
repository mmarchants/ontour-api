package cl.ontour.backend.api.models.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Programa implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_programa")
    private Long id;
    private String codigo;
    private String nombre;
    private String detalle;
    private int valor;
    private boolean estado;    
    @ManyToMany
    @JoinTable(name = "hospedaje_programa", 
            joinColumns = { @JoinColumn(name = "id_programa") },
            inverseJoinColumns = { @JoinColumn(name = "id_hospedaje") })
    private List<Hospedaje> hospedajes;
    @ManyToMany
    @JoinTable(name = "servicio_programa", 
            joinColumns = { @JoinColumn(name = "id_programa") },
            inverseJoinColumns = { @JoinColumn(name = "id_servicio") })
    private List<Servicio> servicios;
    @ManyToMany
    @JoinTable(name = "traslado_programa", 
            joinColumns = { @JoinColumn(name = "id_programa") },
            inverseJoinColumns = { @JoinColumn(name = "id_traslado") })
    private List<Traslado> traslados;
        
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    public List<Hospedaje> getHospedajes() {
        return hospedajes;
    }

    public void setHospedajes(List<Hospedaje> hospedajes) {
        this.hospedajes = hospedajes;
    }
    
    public List<Servicio> getServicios() {
        return servicios;
    }

    public void setServicios(List<Servicio> servicios) {
        this.servicios = servicios;
    }
    
    public List<Traslado> getTraslados() {
        return traslados;
    }

    public void setTraslados(List<Traslado> traslados) {
        this.traslados = traslados;
    }
    
    private static final long serialVersionUID = 1L;
}
