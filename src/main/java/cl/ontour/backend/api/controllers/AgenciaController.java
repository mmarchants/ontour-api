package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.Agencia;
import cl.ontour.backend.api.models.services.IAgenciaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/agencia")
public class AgenciaController {
    
    @Autowired
    private IAgenciaService service;
    
    @GetMapping
    public List<Agencia> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Agencia consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/codigo/{codigo}")
    public Agencia consultarCodigo(@PathVariable(value = "codigo") String codigo) {
        return service.consultarCodigo(codigo);
    }
    
    @PostMapping
    public String registrar(@RequestBody Agencia agencia) {
        return service.registrar(agencia);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody Agencia agencia) {
        return service.modificar(id, agencia);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}