package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.TipoTraslado;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ITipoTrasladoRepository extends CrudRepository<TipoTraslado, Long>{
    
    @Query("SELECT t FROM TipoTraslado t WHERE UPPER(t.codigo) = :codigo")
    public TipoTraslado findByCodigo(@Param("codigo") String codigo);  
    
}
