package cl.ontour.backend.api.models.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Traslado implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_traslado")
    private Long id;
    private String codigo;
    private String nombre;
    private String detalle;
    @Temporal(TemporalType.DATE)
    private Date fecha_partida;
    @Temporal(TemporalType.DATE)    
    private Date fecha_llegada;
    @ManyToOne
    @JoinColumn(name = "id_origen", referencedColumnName = "id_destino")
    private Destino origen;
    @ManyToOne
    @JoinColumn(name = "id_destino", referencedColumnName = "id_destino")
    private Destino destino;
    @ManyToOne
    @JoinColumn(name = "id_tipo_traslado", referencedColumnName = "id_tipo_traslado")
    private TipoTraslado tipo_traslado;
    private boolean estado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Date getFecha_partida() {
        return fecha_partida;
    }

    public void setFecha_partida(Date fecha_partida) {
        this.fecha_partida = fecha_partida;
    }

    public Date getFecha_llegada() {
        return fecha_llegada;
    }

    public void setFecha_llegada(Date fecha_llegada) {
        this.fecha_llegada = fecha_llegada;
    }

    public Destino getOrigen() {
        return origen;
    }

    public void setOrigen(Destino origen) {
        this.origen = origen;
    }

    public Destino getDestino() {
        return destino;
    }

    public void setDestino(Destino destino) {
        this.destino = destino;
    }

    public TipoTraslado getTipo_traslado() {
        return tipo_traslado;
    }

    public void setTipo_traslado(TipoTraslado tipo_traslado) {
        this.tipo_traslado = tipo_traslado;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    private static final long serialVersionUID = 1L;
}
