package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Empleado;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IEmpleadoRepository extends CrudRepository<Empleado, Long> {
    
    @Query("SELECT e FROM Empleado e WHERE e.usuario.correo_electronico = :correo")
    public Empleado findByUsuario(@Param("correo") String correo_electronico);
    
}
