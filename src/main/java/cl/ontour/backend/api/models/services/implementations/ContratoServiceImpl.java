package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Contrato;
import cl.ontour.backend.api.models.repositories.IContratoRepository;
import cl.ontour.backend.api.models.services.IContratoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContratoServiceImpl implements IContratoService {
    
    @Autowired
    private IContratoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Contrato> listar() {
        return (List<Contrato>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Contrato consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Contrato consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Contrato contrato) {
        repository.save(contrato);
        if (repository.findById(contrato.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Contrato contrato) {
        Contrato nuevo_contrato = consultar(id);
        nuevo_contrato.setCodigo(contrato.getCodigo());
        nuevo_contrato.setDescripcion(contrato.getDescripcion());
        nuevo_contrato.setFecha_suscripcion(contrato.getFecha_suscripcion());
        nuevo_contrato.setTipo_contrato(contrato.getTipo_contrato());
        nuevo_contrato.setEstado(contrato.getEstado());
        return registrar(nuevo_contrato);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Contrato contrato = consultar(id);
        contrato.setEstado(false);
        return registrar(contrato);
    }
}
