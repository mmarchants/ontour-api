package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Hospedaje;
import cl.ontour.backend.api.models.repositories.IHospedajeRepository;
import cl.ontour.backend.api.models.services.IHospedajeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class HospedajeServiceImpl implements IHospedajeService {
    
    @Autowired
    private IHospedajeRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Hospedaje> listar() {
        return (List<Hospedaje>)repository.findAll();
                
    }

    @Override
    @Transactional(readOnly = true)
    public Hospedaje consultar(Long id) {
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(Hospedaje hospedaje) {
        repository.save(hospedaje);
        if (repository.findById(hospedaje.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Hospedaje hospedaje) {
        Hospedaje nuevo_hospedaje = consultar(id);
        nuevo_hospedaje.setCodigo(hospedaje.getCodigo());
        nuevo_hospedaje.setNombre(hospedaje.getNombre());
        nuevo_hospedaje.setDetalle(hospedaje.getDetalle());
        nuevo_hospedaje.setFecha_ingreso(hospedaje.getFecha_ingreso());
        nuevo_hospedaje.setFecha_salida(hospedaje.getFecha_salida());
        nuevo_hospedaje.setDestino(hospedaje.getDestino());
        nuevo_hospedaje.setEstado(hospedaje.getEstado());
        return registrar(nuevo_hospedaje);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Hospedaje hospedaje = consultar(id);
        hospedaje.setEstado(false);
        return registrar(hospedaje);
    }

    @Override
    @Transactional(readOnly = true)
    public Hospedaje consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }
}
