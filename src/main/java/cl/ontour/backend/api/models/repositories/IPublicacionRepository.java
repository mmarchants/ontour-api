package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Publicacion;
import org.springframework.data.repository.CrudRepository;

public interface IPublicacionRepository extends CrudRepository<Publicacion, Long> {
    
}
