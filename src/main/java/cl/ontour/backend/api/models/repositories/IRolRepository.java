package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Rol;
import org.springframework.data.repository.CrudRepository;

public interface IRolRepository extends CrudRepository<Rol, Long> {
    
}
