package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.TipoSeguro;
import java.util.List;

public interface ITipoSeguroService {
    
    public List<TipoSeguro> listar();
    public TipoSeguro consultar(Long id);    
    public String registrar(TipoSeguro tipo_seguro);    
    public String modificar(Long id, TipoSeguro tipo_seguro);    
    public String eliminar(Long id);
    public TipoSeguro consultarCodigo(String codigo);
}
