package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Agencia;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IAgenciaRepository extends CrudRepository<Agencia, Long> {
    
    @Query("SELECT a FROM Agencia a WHERE UPPER(a.codigo) = :codigo")
    public Agencia findByCodigo(@Param("codigo") String codigo);
    
}
