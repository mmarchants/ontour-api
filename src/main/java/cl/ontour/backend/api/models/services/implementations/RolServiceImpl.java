package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Rol;
import cl.ontour.backend.api.models.repositories.IRolRepository;
import cl.ontour.backend.api.models.services.IRolService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RolServiceImpl implements IRolService {
    
    @Autowired
    private IRolRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Rol> listar() {
        return (List<Rol>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Rol consultar(Long id) {
        return repository.findById(id).get();
    }

    @Override
    @Transactional
    public String registrar(Rol rol) {
        repository.save(rol);
        if (repository.findById(rol.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Rol rol) {
        Rol nuevo_rol = consultar(id);
        nuevo_rol.setCodigo(rol.getCodigo());
        nuevo_rol.setNombre(rol.getNombre());
        nuevo_rol.setEstado(rol.getEstado());
        return registrar(nuevo_rol);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Rol rol = consultar(id);
        rol.setEstado(false);
        return registrar(rol);
    }
}
