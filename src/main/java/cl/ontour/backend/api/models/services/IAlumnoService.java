package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Alumno;
import java.util.List;

public interface IAlumnoService {
    
    public List<Alumno> listar();
    public Alumno consultar(Long id);    
    public String registrar(Alumno alumno);    
    public String modificar(Long id, Alumno alumno);    
    public String eliminar(Long id);
    
}
