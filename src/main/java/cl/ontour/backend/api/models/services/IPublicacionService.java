package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Publicacion;
import java.util.List;

public interface IPublicacionService {
    
    public List<Publicacion> listar();
    public Publicacion consultar(Long id);    
    public String registrar(Publicacion publicacion);    
    public String modificar(Long id, Publicacion publicacion);    
    public String eliminar(Long id);
    
}
