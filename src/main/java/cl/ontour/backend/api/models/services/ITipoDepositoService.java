package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.TipoDeposito;
import java.util.List;

public interface ITipoDepositoService {
    
    public List<TipoDeposito> listar();
    public TipoDeposito consultar(Long id);    
    public String registrar(TipoDeposito tipo_deposito);    
    public String modificar(Long id, TipoDeposito tipo_deposito);    
    public String eliminar(Long id);
    public TipoDeposito consultarCodigo(String codigo);
}
