package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Hospedaje;
import java.util.List;

public interface IHospedajeService {
    
    public List<Hospedaje> listar();
    public Hospedaje consultar(Long id);    
    public String registrar(Hospedaje hospedaje);    
    public String modificar(Long id, Hospedaje hospedaje);    
    public String eliminar(Long id);
    public Hospedaje consultarCodigo(String codigo);
}
