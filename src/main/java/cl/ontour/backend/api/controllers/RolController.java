package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.Rol;
import cl.ontour.backend.api.models.services.IRolService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/rol")
public class RolController {
    
    @Autowired
    private IRolService service;
    
    @GetMapping
    public List<Rol> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Rol consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @PostMapping
    public String registrar(@RequestBody Rol rol) {
        return service.registrar(rol);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody Rol rol) {
        return service.modificar(id, rol);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
