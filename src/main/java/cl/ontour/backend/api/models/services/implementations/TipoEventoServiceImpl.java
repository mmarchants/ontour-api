package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.TipoEvento;
import cl.ontour.backend.api.models.repositories.ITipoEventoRepository;
import cl.ontour.backend.api.models.services.ITipoEventoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TipoEventoServiceImpl implements ITipoEventoService {
    
    @Autowired
    private ITipoEventoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<TipoEvento> listar() {
        return (List<TipoEvento>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public TipoEvento consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public TipoEvento consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(TipoEvento tipo_evento) {
        repository.save(tipo_evento);
        if (repository.findById(tipo_evento.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, TipoEvento tipo_evento) {
        TipoEvento nuevo_tipo_evento = consultar(id);
        nuevo_tipo_evento.setCodigo(tipo_evento.getCodigo());
        nuevo_tipo_evento.setNombre(tipo_evento.getNombre());
        nuevo_tipo_evento.setDescripcion(tipo_evento.getDescripcion());
        nuevo_tipo_evento.setEstado(tipo_evento.getEstado());
        return registrar(nuevo_tipo_evento);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        TipoEvento tipo_evento = consultar(id);
        tipo_evento.setEstado(false);
        return registrar(tipo_evento);
    }
    
    
    
}
