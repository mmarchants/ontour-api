package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Pais;
import cl.ontour.backend.api.models.services.IPaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cl.ontour.backend.api.models.repositories.IPaisRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PaisServiceImpl implements IPaisService {
    
    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    private IPaisRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Pais> listar() {        
        StoredProcedureQuery sp = em.createStoredProcedureQuery("pkg_pais.prc_list_pais", Pais.class);
        sp.registerStoredProcedureParameter(1, void.class, ParameterMode.REF_CURSOR);
        sp.execute();
        List<Pais> paises = (List<Pais>) sp.getResultList();
        return paises;
    }

    @Override
    @Transactional(readOnly = true)
    public Pais consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Pais consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Pais pais) {
        repository.save(pais);
        if (repository.findById(pais.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }

    @Override
    @Transactional
    public String modificar(Long id, Pais pais) {
        Pais nuevo_pais = consultar(id);        
        nuevo_pais.setCodigo(pais.getCodigo());
        nuevo_pais.setNombre(pais.getNombre());
        nuevo_pais.setEstado(pais.getEstado());
        return registrar(nuevo_pais);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Pais pais = consultar(id);
        pais.setEstado(false);
        return registrar(pais);
    }
}