package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Traslado;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ITrasladoRepository extends CrudRepository<Traslado, Long>{
    
    @Query("SELECT t FROM Traslado t WHERE UPPER(t.codigo) = :codigo")
    public Traslado findByCodigo(@Param("codigo") String codigo);  
    
}
