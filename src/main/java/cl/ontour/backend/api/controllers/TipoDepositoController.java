package cl.ontour.backend.api.controllers;

import cl.ontour.backend.api.models.entities.TipoDeposito;
import cl.ontour.backend.api.models.services.ITipoDepositoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/tipo_deposito")
public class TipoDepositoController {
    
    @Autowired
    private ITipoDepositoService service;
    
    @GetMapping
    public List<TipoDeposito> listar() {
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public TipoDeposito consultar(@PathVariable(value = "id") Long id) {
        return service.consultar(id);
    }
    
    @GetMapping("/codigo/{codigo}")
    public TipoDeposito consultarCodigo(@PathVariable(value = "codigo") String codigo) {
        return service.consultarCodigo(codigo);
    }
    
    @PostMapping
    public String registrar(@RequestBody TipoDeposito tipo_deposito) {
        return service.registrar(tipo_deposito);
    }
    
    @PutMapping("/{id}")
    public String modificar(@PathVariable(value = "id") Long id, @RequestBody TipoDeposito tipo_deposito) {
        return service.modificar(id, tipo_deposito);
    }
    
    @DeleteMapping("/{id}")
    public String eliminar(@PathVariable(value = "id") Long id) {
        return service.eliminar(id);
    }
}
