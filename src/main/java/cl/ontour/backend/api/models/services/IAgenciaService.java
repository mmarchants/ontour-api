package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Agencia;
import java.util.List;

public interface IAgenciaService {
    
    public List<Agencia> listar();    
    public Agencia consultar(Long id);    
    public String registrar(Agencia agencia);
    public String modificar(Long id, Agencia agencia);
    public String eliminar(Long id);
    public Agencia consultarCodigo(String codigo);
    
}
