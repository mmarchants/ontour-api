package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Colegio;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IColegioRepository extends CrudRepository<Colegio, Long> {
    
    @Query("SELECT c FROM Colegio c WHERE UPPER(c.codigo) = :codigo")
    public Colegio findByCodigo(@Param("codigo") String codigo);    
    
}
