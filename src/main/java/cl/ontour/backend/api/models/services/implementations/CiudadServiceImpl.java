package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Ciudad;
import cl.ontour.backend.api.models.repositories.ICiudadRepository;
import cl.ontour.backend.api.models.services.ICiudadService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CiudadServiceImpl implements ICiudadService {
    
    @Autowired
    private ICiudadRepository repository;
    
    @Override
    @Transactional(readOnly = true)
    public List<Ciudad> listar() {
        return (List<Ciudad>)repository.findAll();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Ciudad consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Ciudad consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }
    
    @Override
    @Transactional
    public String registrar(Ciudad ciudad) {
        repository.save(ciudad);
        if (repository.findById(ciudad.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }
    
    @Override
    @Transactional
    public String modificar(Long id, Ciudad ciudad) {
        Ciudad nueva_ciudad = consultar(id);
        nueva_ciudad.setCodigo(ciudad.getCodigo());
        nueva_ciudad.setNombre(ciudad.getNombre());        
        nueva_ciudad.setPais(ciudad.getPais());
        nueva_ciudad.setEstado(ciudad.getEstado());
        return registrar(nueva_ciudad);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Ciudad ciudad = consultar(id);
        ciudad.setEstado(false);
        return registrar(ciudad);
    }
}
