package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Pais;
import java.util.List;

public interface IPaisService {
    
    public List<Pais> listar();
    public Pais consultar(Long id);    
    public String registrar(Pais pais);    
    public String modificar(Long id, Pais pais);    
    public String eliminar(Long id);
    public Pais consultarCodigo(String codigo);
    
}
