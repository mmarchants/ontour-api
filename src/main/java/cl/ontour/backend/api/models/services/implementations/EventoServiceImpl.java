package cl.ontour.backend.api.models.services.implementations;

import cl.ontour.backend.api.models.entities.Evento;
import cl.ontour.backend.api.models.repositories.IEventoRepository;
import cl.ontour.backend.api.models.services.IEventoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EventoServiceImpl implements IEventoService {
    
    @Autowired
    private IEventoRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Evento> listar() {
        return (List<Evento>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Evento consultar(Long id) {
        return repository.findById(id).get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Evento consultarCodigo(String codigo) {
        return repository.findByCodigo(codigo.toUpperCase());
    }

    @Override
    @Transactional
    public String registrar(Evento evento) {
        repository.save(evento);
        if (repository.findById(evento.getId()) != null) {
            return "Ok";
        }
        return "Error";
    }
    
    @Override
    @Transactional
    public String modificar(Long id, Evento evento) {
        Evento nuevo_evento = consultar(id);
        nuevo_evento.setCodigo(evento.getCodigo());
        nuevo_evento.setDetalle(evento.getDetalle());
        nuevo_evento.setGanancia(evento.getGanancia());
        nuevo_evento.setFecha_realizacion(evento.getFecha_realizacion());
        nuevo_evento.setApoderado(evento.getApoderado());
        nuevo_evento.setReserva(evento.getReserva());
        nuevo_evento.setDeposito(evento.getDeposito());
        nuevo_evento.setTipo_evento(evento.getTipo_evento());
        nuevo_evento.setEstado(evento.getEstado());
        return registrar(nuevo_evento);
    }

    @Override
    @Transactional
    public String eliminar(Long id) {
        Evento evento = consultar(id);
        evento.setEstado(false);
        return registrar(evento);
    }
    
    
    
}
