package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Rol;
import java.util.List;

public interface IRolService {
    
    public List<Rol> listar();
    public Rol consultar(Long id);    
    public String registrar(Rol rol);    
    public String modificar(Long id, Rol rol);    
    public String eliminar(Long id);
    
}
