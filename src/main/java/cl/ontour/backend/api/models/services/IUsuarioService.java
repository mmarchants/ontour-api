package cl.ontour.backend.api.models.services;

import cl.ontour.backend.api.models.entities.Usuario;
import java.util.List;

public interface IUsuarioService {
    
    public List<Usuario> listar();
    public Usuario consultar(Long id);    
    public String registrar(Usuario usuario);    
    public String modificar(Long id, Usuario usuario);    
    public String eliminar(Long id);
    public Usuario obtenerPorCorreo(String correo_electronico);
    
}
