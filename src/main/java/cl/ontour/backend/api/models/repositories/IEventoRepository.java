package cl.ontour.backend.api.models.repositories;

import cl.ontour.backend.api.models.entities.Evento;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IEventoRepository extends CrudRepository<Evento, Long> {
    @Query("SELECT e FROM Evento e WHERE UPPER(e.codigo) = :codigo")
    public Evento findByCodigo(@Param("codigo") String codigo);  
}
